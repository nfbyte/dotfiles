### Technologies
- [Alpine Linux](https://alpinelinux.org)
- [Sway](https://swaywm.org)
- [Firefox](https://mozilla.org/firefox)
* * *
### Installation
#### Requirements
- `git`
#### Instructions
1. Create local git repository:
```sh
git init --initial-branch=main
```
2. Add remote:
```sh
git remote add origin https://gitlab.com/nfbyte/dotfiles
```
3. Download remote files:
```sh
git pull origin main
```
