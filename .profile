# environment variables
export LC_ALL="C.UTF-8"
export NIX_REMOTE=""
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_DATA_DIRS="$XDG_DATA_HOME:/usr/share"
export PATH="${PATH#$HOME/.nix-profile/bin:/nix/var/nix/profiles/default/bin:}"
export PATH="$PATH:$HOME/.cargo/bin"
#export PATH="$PATH:$HOME/.talos/cni/bin"

# startup
sway
