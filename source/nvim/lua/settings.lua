-- Enable standard keybindings (Ctrl+C, Ctrl+V, Ctrl+X, Ctrl+Z)
vim.cmd("runtime mswin.vim")

-- Prefer dark theme
vim.opt.background = "dark"

-- Theme
vim.cmd("colorscheme tokyonight")

-- GUI font setting
vim.opt.guifont = "Monospace:h12"

-- Unified status line
vim.opt.laststatus = 3

-- Hide command line
vim.opt.cmdheight = 0

-- Fix cursor shape
vim.opt.guicursor = "a:blinkon0,n-v:block,i-c:ver25"

-- Allow cursor to go one character past end of line
vim.opt.virtualedit = "onemore"

-- Don't include that one character in selection
vim.opt.selection = "old"

-- Fix copy & paste
vim.opt.clipboard:append("unnamedplus")

-- Hide tilde spam on signcolumn
vim.opt.fillchars = {eob = " "}

-- Don't show the signcolumn
vim.opt.signcolumn = "no"

-- Show line numbers
vim.opt.number = true

-- Show tabs
vim.opt.showtabline = 2

-- Set whitespace indicators
vim.opt.listchars = {space = "·", lead = " ", tab = "> "}

-- Show whitespace indicators
vim.opt.list = true

-- Fix buffer behaviour
vim.opt.hidden = true

-- Don't automatically resize windows
vim.opt.equalalways = false

-- Fix rendering glitches
vim.opt.lazyredraw = true

-- Fix cursor behaviour
vim.opt.startofline = false

-- Enable command mode completions
vim.opt.wildmenu = true

-- Disable backups
vim.opt.backup = false

-- Disable backups
vim.opt.writebackup = false

-- Disable swap files
vim.opt.swapfile = false

-- Fix delay
vim.opt.updatetime = 300

-- Highlight cursor line
vim.opt.cursorline = true

-- Keep cursor line centered
vim.opt.scrolloff = 24

-- Expand folds by default
vim.opt.foldenable = false

-- Don't highlight search results
vim.opt.hlsearch = false

-- Don't jump to search result before ending search
vim.opt.incsearch = false

-- Set default indentation level for spaces
vim.opt.shiftwidth = 2

-- Set default indentation level for tabs
vim.opt.tabstop = 2

-- Don't insert spaces instead of tabs
vim.opt.expandtab = false

-- Don't add a newline at the end of a file
vim.opt.fixendofline = false

-- Make search case insensitive
vim.opt.ignorecase = true

-- Enable terminal emulator window title
vim.opt.title = true

-- Show file name in terminal emulator window title
vim.opt.titlestring = [[%t – %{fnamemodify(getcwd(), ':t')}]]
