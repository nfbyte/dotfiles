-- Strip any trailing newline from clipboard
vim.api.nvim_create_autocmd("TextYankPost", {
   pattern = "*",
   callback = function()
      vim.cmd([[let @"=substitute(@+,'\n$','','g')]])
   end
})

-- Don't modify cursor shape on exit
vim.api.nvim_create_autocmd("VimLeave", {
   pattern = "*",
   callback = function()
      vim.opt.guicursor = "a:ver100"
   end
})

-- Set current directory to current directory
vim.api.nvim_create_autocmd("BufEnter", {
   pattern = "*",
   callback = function()
      vim.cmd("silent! lcd %:p:h")
   end
})

-- Disable automatic indendation
vim.api.nvim_create_autocmd("BufWinEnter", {
   pattern = "*",
   callback = function()
      vim.opt.formatoptions = ""
      vim.opt.autoindent = false
      vim.opt.cindent = false
      vim.opt.smartindent = false
      vim.opt.indentexpr = ""
   end
})

-- Language-specific settings: Lua
vim.api.nvim_create_autocmd("FileType", {
   pattern = "lua",
   callback = function()
      vim.opt.shiftwidth = 3
      vim.opt.tabstop = 3
      vim.opt.expandtab = true
   end
})

-- Language-specific settings: LaTeX
vim.api.nvim_create_autocmd("FileType", {
   pattern = "tex",
   callback = function()
      vim.opt.shiftwidth = 2
      vim.opt.tabstop = 2
   end
})
