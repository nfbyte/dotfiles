require("nvim-treesitter.configs").setup({highlight = {enable = true}})
require("tokyonight").setup({style = "night", transparent = true})
--require("catppuccin").setup({transparent_background = true})
require("lualine").setup({tabline = {lualine_a = {"buffers"}}})
--require("lualine").setup()
require("tabline").setup()

require("telescope").setup({
   defaults = {
      mappings = {
         i = { ["<esc>"] = require("telescope.actions").close },
      },
      layout_strategy = "horizontal",
      layout_config = {
         width = 0.92,
         preview_width = 0.65
      }
   },
   pickers = {
      find_files = {
         hidden = true,
         find_command = { "fd", "--type", "file", "--strip-cwd-prefix" }
      }
   }
})

require("noice").setup({
  presets = {
    command_palette = true,
    bottom_search = true,
  },
  --[[lsp = {
    -- override markdown rendering so that **cmp** and other plugins use **Treesitter**
    override = {
      ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
      ["vim.lsp.util.stylize_markdown"] = true,
      ["cmp.entry.get_documentation"] = true, -- requires hrsh7th/nvim-cmp
    }
  }]]--
})

--[[require("cmp").setup({
   mapping = {
     ["<Tab>"] = require("cmp").mapping(function(fallback)
       if require("cmp").visible() then
         require("cmp").select_next_item()
       else
         fallback()
       end
     end, { "i", "" }),
     ["<S-Tab>"] = require("cmp").mapping.select_prev_item(),
   },
   sources = {
      { name = 'path' } -- file previews for this are slow as fuck
   },
   snippet = {
      -- REQUIRED - you must specify a snippet engine
      expand = function(args)
      -- require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
      -- vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
      -- require('snippy').expand_snippet(args.body) -- For `snippy` users.
      -- vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
         vim.snippet.expand(args.body) -- For native neovim snippets (Neovim v0.10+)
      end,
   },
   window = {
   -- completion = cmp.config.window.bordered(),
   -- documentation = cmp.config.window.bordered(),
   },
--  mapping = mapping.preset.insert({
--    ['<C-b>'] = mapping.scroll_docs(-4),
--    ['<C-f>'] = mapping.scroll_docs(4),
--    ['<C-Space>'] = mapping.complete(),
--    ['<C-e>'] = mapping.abort(),
--    ['<CR>'] = mapping.confirm({ select = true }),
--  }),
--  config = {
--    sources({
--      { name = 'nvim_lsp' },
--    -- { name = 'vsnip' }, -- For vsnip users.
    -- { name = 'luasnip' }, -- For luasnip users.
    -- { name = 'ultisnips' }, -- For ultisnips users.
    -- { name = 'snippy' }, -- For snippy users.
--    }, {
--      { name = 'buffer' },
--    })
--  }
})
]]--

--require('orgmode').setup({mappings = {disable_all = true}})

--require("nvim-tree").setup()

--require("lspconfig").clangd.setup({})
--require("lspconfig").rust_analyzer.setup({})

--[===============[
local cmp = require("cmp")
cmp.setup({
   mapping = cmp.mapping.preset.insert({
      ["<C-b>"] = cmp.mapping.scroll_docs(-4),
      ["<C-f>"] = cmp.mapping.scroll_docs(4),
      ["<C-Space>"] = cmp.mapping.complete(),
      ["<C-e>"] = cmp.mapping.abort(),
      ["<Tab>"] = cmp.mapping.confirm({select = true}),
   }),
   sources = cmp.config.sources({
      {name = "nvim_lsp"},
      {name = "path"},
   })
})

local capabilities = require("cmp_nvim_lsp").update_capabilities(vim.lsp.protocol.make_client_capabilities())
require("lspconfig").rust_analyzer.setup({capabilities = capabilities})
--]===============]

--[===============[
if (vim.cmd([[exists('g:started_by_firenvim')]])) then
   vim.opt.showtabline = 0
   vim.opt.laststatus = 0
end
--]===============]

-- Firenvim
vim.g.firenvim_config = {
  globalSettings = {
    alt = "all", -- Allow usage of Alt key for all sites
  },
  localSettings = {
    -- Replace with your intended websites
    ["https?://(www%.example1%.com|www%.example2%.com)"] = {
      takeover = "always", -- Enable Firenvim takeover on these sites
      cmdline = "neovim", -- Use Neovim's command line
      content = "text", -- Read content as text
      priority = 1, -- Higher priority for these patterns
    },
    [".*"] = { 
      takeover = "never", -- Disable takeover on all other sites
      cmdline = "none", -- No command line for other sites
      content = "text",
      priority = 0, -- Lower priority for all other patterns
    },
  },
}

-- Firenvim - Kaggle
vim.api.nvim_create_autocmd({'BufEnter'}, {
    pattern = "duction.jupyter-proxy.kaggle.net_*.txt",
    callback = function()
       vim.opt.filetype = "python"
    end
})
