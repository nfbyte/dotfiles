-- Move cursor 1 line up
vim.keymap.set("", "i", "gk")

-- Move cursor 1 line down
vim.keymap.set("", "k", "gj")

-- Move cursor 1 character back
vim.keymap.set("", "j", "h")

-- Move cursor 1 character forward
--vim.keymap.set("", "l", "l")

-- Select characters (enter visual mode)
vim.keymap.set("", "a", "v")

-- Select line (enter visual line mode)
vim.keymap.set("", "t", "V")

-- Move cursor 1 "word" back
vim.keymap.set("", "u", "b")

-- Move cursor 1 "word" forward (normal mode) - if the first word in a file is 1 character long it skips over it
vim.keymap.set("n", "o", "<cmd>silent! normal! <Left>e<Right><Enter>")

-- Move cursor 1 "word" forward (visual mode)
vim.keymap.set("v", "o", "e")

-- Move cursor to start of word
--vim.keymap.set("", "U", "B")

-- Move cursor to end of word (normal mode)
--vim.keymap.set("n", "O", "El")

-- Move cursor to end of word (visual mode)
--vim.keymap.set("v", "O", "E")

-- ???
--vim.keymap.set("", "I", "<C-U>")

-- ???
--vim.keymap.set("", "K", "<C-D>")

-- Move cursor 1 paragraph up
vim.keymap.set("n", "I", "{")

-- Move cursor 1 paragraph down
vim.keymap.set("n", "K", "}")

-- Move cursor to start of paragraph (visual mode)
vim.keymap.set("v", "I", "{w_")

-- Move cursor to end of paragraph (visual mode)
vim.keymap.set("v", "K", "}bg_")

-- Move cursor to top
vim.keymap.set("", "8", "gg")

-- Move cursor to bottom
vim.keymap.set("", ",", "G")

-- Undo
vim.keymap.set("", "z", "u")

-- Redo
vim.keymap.set("", "Z", "<C-R>")

-- ???
vim.keymap.set("", "(", "gt")

-- ???
vim.keymap.set("", ")", "gT")

-- Move cursor to beginning of line
vim.keymap.set("", "m", "_")

-- Move cursor to end of line (normal mode)
vim.keymap.set("n", ".", "$l")

-- Move cursor to end of line (Visual mode)
vim.keymap.set("v", ".", "g_l")

-- Enter visual block mode downwards
vim.keymap.set("n", "<C-A-k>", "<C-v>j")

-- Enter insert mode (normal mode)
vim.keymap.set("n", "<Space>", "i")

-- Enter normal mode (insert mode)
vim.keymap.set("i", "<Esc>", "<Esc>`^")

-- Enter insert mode (visual mode)
vim.keymap.set("v", "<Space>", [["_c]])

-- Copy
vim.keymap.set("v", "c", [[ygv<Esc><cmd>call system("wl-copy", @")<Enter>]])

-- Copy
vim.keymap.set("v", "C", [[ygv<Esc><cmd>call system("wl-copy", @")<Enter>]])

-- Paste
vim.keymap.set("", "d", '"+P`]l')

-- Paste (visual mode)
--vim.keymap.set("v", "d", [[<Esc>"=system("wl-paste --no-newline")<Enter>gvpgvy`<]])

-- Cut (copy & delete)
vim.keymap.set("v", "x", [[d<Esc><cmd>call system("wl-copy", @")<Enter>]])

-- Delete current line
vim.keymap.set("n", "<BS>", "cc")

-- Enter insert mode on a new line
vim.keymap.set("n", "<Enter>", "$a<Enter>")

-- Search
vim.keymap.set("n", "f", "/")

-- Search selection
vim.keymap.set("v", "f", [[y<Esc>/<C-r>"<Enter>]])

-- Select word
vim.keymap.set("n", "r", "<cmd>silent! normal! lbvwge<Enter>")

-- Replace all selection instances
vim.keymap.set("v", "r", [["hy:%s/<C-r>h//g<left><left>]])

-- Move cursor to previous search result
vim.keymap.set("n", "7", "N")

-- Move cursor to next search result
vim.keymap.set("n", "9", "n")

-- ???
vim.keymap.set("n", "-", "<C-w>li")

-- Save
vim.keymap.set("n", "h", "<cmd>w<Enter>")

-- Quit
vim.keymap.set("n", "'", "<cmd>q<Enter>")

-- Previous tab
vim.keymap.set("", "1", "<cmd>bp<Enter>", {silent = true})

-- Next tab
vim.keymap.set("", "2", "<cmd>bn<Enter>", {silent = true})

-- Close tab
vim.keymap.set("", "3", "<cmd>bp<bar>sp<bar>bn<bar>bd!<Enter>", {silent = true})

-- Find files
vim.keymap.set("", ";", "<cmd>Telescope find_files<Enter>")

-- Trigger auto-completion (insert mode)
--vim.keymap.set("i", "<C-Space>", require("cmp").mapping.complete())

-- Trigger auto-completion (normal mode)
--vim.keymap.set("n", "<Tab>", "<Space><C-Space>", {remap = true})
