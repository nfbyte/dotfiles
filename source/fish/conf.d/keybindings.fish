# Update prompt before executing command
#bind --mode insert enter repaint execute

# Enter default (normal) mode
bind --mode insert --sets-mode default escape repaint-mode

# Update prompt before executing command (from normal mode)
bind --mode default --sets-mode insert enter repaint execute

# Enter insert mode
bind --mode default --sets-mode insert space repaint-mode

# Delete current line
bind --mode default --sets-mode insert backspace kill-whole-line repaint

# Trigger autocompletion (from normal mode)
bind --mode default --sets-mode insert tab complete repaint-mode

# Trigger history search
bind --mode default --sets-mode insert f history-pager repaint-mode

# Paste from clipboard
bind --mode default d "set fish_cursor_end_mode exclusive" fish_clipboard_paste

# Move cursor 1 character back
bind --mode default j backward-char

# Move cursor 1 character forward (or accept autosuggestion)
bind --mode default l forward-char

# Autocomplete previous command
bind --mode default i "set fish_cursor_end_mode exclusive" history-prefix-search-backward

# Autocomplete next command
bind --mode default k history-prefix-search-forward

# Move cursor 1 "word" back
bind --mode default u backward-word

# Move cursor 1 "word" forward
bind --mode default o "set fish_cursor_end_mode exclusive" forward-word

# Move cursor 1 word back
#bind --mode default U backward-bigword

# Move cursor 1 word forward
#bind --mode default O "set fish_cursor_end_mode exclusive" forward-bigword

# Move cursor to beginning of line
bind --mode default m beginning-of-line

# Move cursor to end of line and accept autosuggestion
bind --mode default . "set fish_cursor_end_mode exclusive" end-of-line accept-autosuggestion
