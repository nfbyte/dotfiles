# Disable greeting
set fish_greeting

# Enable Vi mode
set fish_key_bindings fish_vi_key_bindings

# Set the normal and visual mode cursors to a block
set fish_cursor_default block

# Set the insert mode cursor to a line
set fish_cursor_insert line

# Set the replace mode cursor to an underscore
set fish_cursor_replace_one underscore

# Minimize Escape delay
set fish_escape_delay_ms 10

# Minimize sequence delay
#set fish_sequence_key_delay_ms 10

# PATH
#set PATH "$PATH" "$HOME/.local/bin"
